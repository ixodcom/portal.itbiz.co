<?php 
session_start();


if ( !isset($_SESSION['login']) || $_SESSION['login'] !== true) {

if(empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])){

if ( !isset($_SESSION['token'])) {

if ( !isset($_SESSION['fb_access_token'])) {

 header('Location: login/index.php');

exit;
}
}
}
}

?>

<?php
    include('xcrud-1.6.19/xcrud.php');
?>

<?php 
	$db = Xcrud_db::get_instance();
	$query = 'SELECT * FROM users WHERE username = "'.$_SESSION['username'].'"';								
	$db->query($query);
	$res = $db->result();
	$user_id = $res[0]['id'];

	$query = 'SELECT * FROM user_keys WHERE uid = "'.$user_id.'"';
	$db->query($query);
	$res = $db->result();
	$client_id = $res[0]['cid'];
	$group_id = $res[0]['gid'];
	//used for debug
	//echo $client_id; echo '<--client_id<br>';
	//echo $group_id;  echo '<--group_id<br>';
	//echo $user_id;	 echo '<--user_id<br>';


	if ($group_id == 1) {
		header('Location: admin.php');
	}

	$query = 'SELECT * FROM client WHERE id = "'.$client_id.' LIMIT = 1"';
	$db->query($query);
	$res = $db->result();
	$device_sn = $res[0]['device_sn'];
	$device_model = $res[0]['device_model'];
	$device_url = $res[0]['device_url'];
	$internet_provider = $res[0]['internet_provider'];
	$user_pin = $res[0]['user_pin'];
?>


<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8">		
		<title>Account Portal - ITBiz.co</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="sky-tabs-1.1.2/css/demo.css">
		<link rel="stylesheet" href="sky-tabs-1.1.2/css/font-awesome.css">
		<link rel="stylesheet" href="sky-tabs-1.1.2/css/sky-tabs.css">
		<link rel="stylesheet" href="sky-tabs-1.1.2/css/sky-tabs-red.css">
		
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="css/sky-tabs-ie8.css">
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
			<script src="js/sky-tabs-ie8.js"></script>
		<![endif]-->
	</head>
	
	<body class="bg-red">

		<div class="body">
		
		<header>
			<span class="headerText">
			<?php echo "Welcome:&nbsp;".$_SESSION['username']."&nbsp;&nbsp;&nbsp;&nbsp;"; ?><a class="logoutLinkText" href="login/logout.php">[ Logout? ]</a>
			</span>
			<h1><img src="images/logo.png" alt="ITBiz.co"/></h1>
		</header>

			<!-- tabs -->
			<div class="sky-tabs sky-tabs-pos-top-left sky-tabs-slide-right sky-tabs-response-to-icons">
				<input type="radio" name="sky-tabs" checked id="sky-tab1" class="sky-tab-content-1">
				<label for="sky-tab1"><span><span><i class="fa fa-globe"></i>Summary</span></span></label>
				
				<input type="radio" name="sky-tabs" id="sky-tab2" class="sky-tab-content-2">
				<label for="sky-tab2"><span><span><i class="fa fa-cogs"></i>White List</span></span></label>
				
				<input type="radio" name="sky-tabs" id="sky-tab3" class="sky-tab-content-3">
				<label for="sky-tab3"><span><span><i class="fa fa-cogs"></i>Keyword Scanning</span></span></label>
				
				<input type="radio" name="sky-tabs" id="sky-tab4" class="sky-tab-content-4">
				<label for="sky-tab4"><span><span><i class="fa fa-cogs"></i>Customizations</span></span></label>

				<input type="radio" name="sky-tabs" id="sky-tab5" class="sky-tab-content-5">
				<label for="sky-tab5"><span><span><i class="fa fa-bolt"></i>Recomendations</span></span></label>
				
				<ul>
					<li class="sky-tab-content-1">					
						<div class="typography">
							<h1>Summary of Account Settings</h1>
							<?php 
								$xcrud_alert = Xcrud::get_instance();
								$xcrud_alert->table('alert');
								$xcrud_alert->where('cid =',$client_id);
								$xcrud_alert->unset_add();
								$xcrud_alert->unset_edit();
								$xcrud_alert->unset_remove();
								//$xcrud_alert->unset_view();
								$xcrud_alert->unset_numbers();
								$xcrud_alert->columns('date,time,ip_addr,device_info,website,website_detail,email_message');
								$xcrud_alert->fields('date,time,ip_addr,device_info,website,website_detail,email_message');
								$xcrud_alert->order_by('id', 'desc');								
								$xcrud_alert->table_name('Your Alerts');
								//$xcrud_alert->start_minimized(true);
								echo $xcrud_alert->render();	
							?>
							
							<?php
								$xcrud_whitelist_sum = Xcrud::get_instance();
								$xcrud_whitelist_sum->table('url_whitelist');	
								$xcrud_whitelist_sum->where('cid =',$client_id);					
								$xcrud_whitelist_sum->unset_add();
								$xcrud_whitelist_sum->unset_edit();
								$xcrud_whitelist_sum->unset_remove();
								$xcrud_whitelist_sum->unset_view();
								$xcrud_whitelist_sum->unset_numbers();
								$xcrud_whitelist_sum->columns('url_whitelist,timestamp');
								$xcrud_whitelist_sum->column_width('timestamp','20%');
								$xcrud_whitelist_sum->column_class('timestamp', 'align-center');							
								$xcrud_whitelist_sum->table_name('Your Whitelist');
								//$xcrud_whitelist_sum->start_minimized(true);
								echo $xcrud_whitelist_sum->render();
							?>

							<?php
								$xcrud_url_scan_sum = Xcrud::get_instance();
								$xcrud_url_scan_sum->table('url_scan');	
								$xcrud_url_scan_sum->where('cid =',$client_id);					
								$xcrud_url_scan_sum->unset_add();
								$xcrud_url_scan_sum->unset_edit();
								$xcrud_url_scan_sum->unset_remove();
								$xcrud_url_scan_sum->unset_view();
								$xcrud_url_scan_sum->unset_numbers();	
								$xcrud_url_scan_sum->unset_limitlist();								
								$xcrud_url_scan_sum->unset_print();
								$xcrud_url_scan_sum->unset_csv();
								$xcrud_url_scan_sum->unset_search();															
								$xcrud_url_scan_sum->relation('status','status_activated','status','status');
								$xcrud_url_scan_sum->relation('lid','keyword_list','id','name','list_type="url"');	
								$xcrud_url_scan_sum->columns('lid,status,timestamp');
								$xcrud_url_scan_sum->highlight('status', '=', 'Yes', '#8DED79');
								$xcrud_url_scan_sum->highlight('status', '=', 'No', '#FF8080');
								$xcrud_url_scan_sum->column_class('status', 'align-center font-bold');
								$xcrud_url_scan_sum->column_width('status','5%');
								$xcrud_url_scan_sum->column_width('timestamp','20%');
								$xcrud_url_scan_sum->column_class('timestamp', 'align-center');							
								$xcrud_url_scan_sum->table_name('URL Keyword Scan List');								
								$xcrud_url_scan_sum->label('lid','List Name');
								//$xcrud_url_scan_sum->start_minimized(true);
								echo $xcrud_url_scan_sum->render();
							?>

							<?php
								$xcrud_content_scan_sum = Xcrud::get_instance();
								$xcrud_content_scan_sum->table('content_scan');	
								$xcrud_content_scan_sum->where('cid =',$client_id);					
								$xcrud_content_scan_sum->unset_add();
								$xcrud_content_scan_sum->unset_edit();
								$xcrud_content_scan_sum->unset_remove();
								$xcrud_content_scan_sum->unset_view();
								$xcrud_content_scan_sum->unset_numbers();	
								$xcrud_content_scan_sum->unset_limitlist();								
								$xcrud_content_scan_sum->unset_print();
								$xcrud_content_scan_sum->unset_csv();
								$xcrud_content_scan_sum->unset_search();															
								$xcrud_content_scan_sum->relation('status','status_activated','status','status');
								$xcrud_content_scan_sum->relation('lid','keyword_list','id','name','list_type="url"');	
								$xcrud_content_scan_sum->columns('lid,status,timestamp');
								$xcrud_content_scan_sum->highlight('status', '=', 'Yes', '#8DED79');
								$xcrud_content_scan_sum->highlight('status', '=', 'No', '#FF8080');
								$xcrud_content_scan_sum->column_class('status', 'align-center font-bold');
								$xcrud_content_scan_sum->column_width('status','5%');
								$xcrud_content_scan_sum->column_width('timestamp','20%');
								$xcrud_content_scan_sum->column_class('timestamp', 'align-center');							
								$xcrud_content_scan_sum->table_name('Content Keyword Scan List');								
								$xcrud_content_scan_sum->label('lid','List Name');
								//$xcrud_content_scan_sum->start_minimized(true);
								echo $xcrud_content_scan_sum->render();
							?>																													
						</div>
					</li>
					
					<li class="sky-tab-content-2">
						<div class="typography">
							<p><b>How it works:  </b>The white list works on the domain name level e.g. example.com.  When a domain name is on the white list, access restrictions are turned off, for example, if the word "porn" is generally blocked, when you add "pornharms.com" to the white list, and if there is an article on pornharms.com that contains the word "porn", users will be allowed to view the content on the whitelisted domain name.</p>
							<p><b>Please note: </b>The domain names support.wordpress.com and wordpress.com are different domain names, if you place "wordpress.com" on the white list you will allow all WordPress blogs, but you may want just "support.wordpress.com" on the white list. There are blogs on WordPress from every walk of life, including pornography blogs, so a best practice is to whitelist only blogs you have an interest in. i.e. 'grandmatomkpkin.wordpress.com'</p>
							<p><b>Usage: </b>Enter any of the following: <b>example.com</b> or <b>blog.example.com</b> or <b>http://school.example.com</b> and we will take <b>only the domain name</b> and place it on the whitelist.</p>
							<?php
								$xcrud_whitelist = Xcrud::get_instance();
								$xcrud_whitelist->table('url_whitelist');
								$xcrud_whitelist->columns('url_whitelist');
								$xcrud_whitelist->fields('url_whitelist');
								$xcrud_whitelist->where('cid =',$client_id);
								$xcrud_whitelist->label('url_whitelist','Domain Name or URL');
								$xcrud_whitelist->table_name('Your Whitelist');
								$xcrud_whitelist->pass_var('cid',$client_id);
								$xcrud_whitelist->before_insert('whitelist_before_insert');
								$xcrud_whitelist->after_insert('whitelist_after_insert');
								$xcrud_whitelist->after_update('whitelist_after_update');
								$xcrud_whitelist->after_remove('whitelist_after_remove');
								$xcrud_whitelist->before_remove('whitelist_before_remove');
								//$xcrud_whitelist->unset_search();
								//$xcrud_whitelist->unset_limitlist();
								$xcrud_whitelist->unset_view();
								$xcrud_whitelist->set_var('client_id',$client_id);
								

								echo $xcrud_whitelist->render('view');
							?>
						</div>
					</li>
					
					<li class="sky-tab-content-3">
						<div class="typography">
							<h1>Keyword Scanning</h1>
							<p><b>How it works: </b>There are two types of keyword scanning, URL scanning and page content scans (Guardian pro only). URL scanning will scan the web address for every page, you can see webpage address at the top of most web browsers.  Blocking works the same on both types of scans, if a word is found that is not allowed, the page or URL is blocked. Page content scans are just what you might imagine; words on a webpage are scanned as they are sent to your network, only the Guardian Pro does page scanning.</p>
							
							<p>Page content scans and URL scans use different keyword list, there are default list you can choose or build your own custom list.</p>
							
							<p><b>Please note: </b>The more aggressive keyword scanning is used, the more you will need to use the whitelist feature.  You may block the word “breast”, however if your favorite news site has an article about “breast cancer” that page will be blocked; unless it is on your “whitelist”. Chances are your favorite news sites are not known for pornography so it is safe to put that site on the  “whitelist” then pages scans and URL scans will be skipped for just that site.</p>
							
							<p><b>Usage: </b>Select the default keyword blocking list and be sure blocking is active.</p>
							<?php
								$xcrud_url_scan = Xcrud::get_instance();
								$xcrud_url_scan->table('url_scan');
								$xcrud_url_scan->unset_limitlist();
								$xcrud_url_scan->unset_view();
								$xcrud_url_scan->unset_print();
								$xcrud_url_scan->unset_sortable();
								$xcrud_url_scan->unset_search();
								$xcrud_url_scan->unset_add();
								$xcrud_url_scan->unset_remove();
								$xcrud_url_scan->unset_csv();
								$xcrud_url_scan->unset_numbers();
								$xcrud_url_scan->columns('lid,status');
								$xcrud_url_scan->fields('lid,status');
								$xcrud_url_scan->label('lid','List Name');
								$xcrud_url_scan->label('status','Activate Scanning?');
								$xcrud_url_scan->label('lid','List Name');
								$xcrud_url_scan->highlight('status', '=', 'Yes', '#8DED79');
								$xcrud_url_scan->highlight('status', '=', 'No', '#FF8080');
								$xcrud_url_scan->column_class('status', 'align-center font-bold');
								$xcrud_url_scan->column_width('status','5%');
								$xcrud_url_scan->table_name('URL Keyword Scanning');
								$xcrud_url_scan->pass_var('cid',$client_id);
								$xcrud_url_scan->after_update('url_after_update');
								//$xcrud_url_scan->start_minimized(true);
								$xcrud_url_scan->where('cid =', $client_id);
								$xcrud_url_scan->relation('status','status_activated','status','status');
								$xcrud_url_scan->relation('lid','keyword_list','id','name','list_type="url" AND attrib="default"');								

								echo $xcrud_url_scan->render('view');
							?>	

							<?php
								if ($device_model == 'UTM5') {
									$xcrud_content_scan = Xcrud::get_instance();
									$xcrud_content_scan->table('content_scan');									
									$xcrud_content_scan->unset_limitlist();
									$xcrud_content_scan->unset_view();
									$xcrud_content_scan->unset_print();
									$xcrud_content_scan->unset_sortable();
									$xcrud_content_scan->unset_search();
									$xcrud_content_scan->unset_add();
									$xcrud_content_scan->unset_remove();
									$xcrud_content_scan->unset_csv();
									$xcrud_content_scan->unset_numbers();								
									$xcrud_content_scan->columns('lid,status');
									$xcrud_content_scan->fields('lid,status');
									$xcrud_content_scan->label('lid','List Name');
									$xcrud_content_scan->label('status','Activate Scanning?');
									$xcrud_content_scan->label('lid','List Name');
									$xcrud_content_scan->highlight('status', '=', 'Yes', '#8DED79');
									$xcrud_content_scan->highlight('status', '=', 'No', '#FF8080');
									$xcrud_content_scan->column_class('status', 'align-center font-bold');
									$xcrud_content_scan->column_width('status','5%');
									$xcrud_content_scan->table_name('Web Page Keyword Scanning');
									$xcrud_content_scan->pass_var('cid',$client_id);
									$xcrud_content_scan->after_update('content_after_update');
									$xcrud_content_scan->where('cid =', $client_id);
									$xcrud_content_scan->relation('status','status_activated','status','status');
									$xcrud_content_scan->relation('lid','keyword_list','id','name','list_type="content" AND attrib="default"');	
									echo $xcrud_content_scan->render('view');
								}
							?>				

						</div>
					</li>
					
					<li class="sky-tab-content-4">
						<div class="typography">
							<h1>Customizations</h1>
							<?php
							if ($device_model == 'UTM5') {
								echo '<p>There are two lists that can be customized, namely the URL scan list, and the content scan list.  The URL Scan List only searches URLs, like what you see in the address bar of your web browser.  The Content Scan List searches webpage content.  The two lists have many of the same words; HOWEVER, they are two separate and distinct lists due to how they are used.</p>';
							}else{
								echo 'The URL Scan List only searches URLs, like what you see in the address bar of your web browser.';
							}
							?>
							
							<p><b>The URL Word List</b>, can only accept single words and may include the “*” wild card, for example “bo*ots” in the list, will catch the words “boots” and “boooots” in the URL block list.</p>
							
							<?php
							if ($device_model == 'UTM5') {
								echo '<p><b>The Content Word List</b> is made up of one word and two word phrases, for example: “boots” or “nice boots”.  There are times when words in a content scan are better blocked with a discriptive term at the beginning or end, for example: “nice boots” or “big boots”. Please note you cannot use wild cards for this list.</p>';
							
								echo '<p><b>Usage:</b> Add custom words or place words from the default lists in the “Remove URL Words or Remove Content Words”</p>';
							}
							?>

							<?php
								$xcrud_custom_url_keywords = Xcrud::get_instance();
								$xcrud_custom_url_keywords->table('custom_url_keywords');
								$xcrud_custom_url_keywords->where('cid =', $client_id);
								$xcrud_custom_url_keywords->where('status <>','delete');
								$xcrud_custom_url_keywords->pass_var('cid',$client_id);
								$xcrud_custom_url_keywords->set_var('cidDelete',$client_id);
								$xcrud_custom_url_keywords->unset_numbers();
								$xcrud_custom_url_keywords->unset_view();
								$xcrud_custom_url_keywords->table_name('Add Words to The URL Word List');
								$xcrud_custom_url_keywords->columns('word,rating,status');
								$xcrud_custom_url_keywords->fields('word,rating,status');
								$xcrud_custom_url_keywords->column_width('rating,status','5%');
								$xcrud_custom_url_keywords->highlight('status', '=', 'Active', '#8DED79');
								$xcrud_custom_url_keywords->highlight('status', '=', 'Inactive', '#FF8080');
								$xcrud_custom_url_keywords->relation('rating','keyword_ratings','rating','rating');
								$xcrud_custom_url_keywords->after_update('url_after_update');
								$xcrud_custom_url_keywords->after_insert('url_after_insert');
								$xcrud_custom_url_keywords->after_remove('url_after_remove');
								$xcrud_custom_url_keywords->relation('status','status_customizations','status','status');	

								echo $xcrud_custom_url_keywords->render('view');
							?>

							<?php
								echo '<b>WARNING: The words on this list are very graphic!</b>';
								$xcrud_custom_url_keywordsDELETE = Xcrud::get_instance();
								$xcrud_custom_url_keywordsDELETE->table('custom_url_keywords');
								$xcrud_custom_url_keywordsDELETE->where('cid =', $client_id);
								$xcrud_custom_url_keywordsDELETE->where('status =','delete');
								$xcrud_custom_url_keywordsDELETE->pass_var('cid',$client_id);
								$xcrud_custom_url_keywordsDELETE->pass_var('status','delete');
								$xcrud_custom_url_keywordsDELETE->set_var('cidDelete',$client_id);
								$xcrud_custom_url_keywordsDELETE->unset_numbers();
								$xcrud_custom_url_keywordsDELETE->unset_view();
								$xcrud_custom_url_keywordsDELETE->unset_edit();
								$xcrud_custom_url_keywordsDELETE->table_name('Remove Words from The URL Word List');
								$xcrud_custom_url_keywordsDELETE->columns('word');
								$xcrud_custom_url_keywordsDELETE->fields('word');
								$xcrud_custom_url_keywordsDELETE->after_update('url_after_update');
								$xcrud_custom_url_keywordsDELETE->after_insert('url_after_insert');
								$xcrud_custom_url_keywordsDELETE->after_remove('url_after_remove');		
								$xcrud_custom_url_keywordsDELETE->relation('word','url_keywords','word','word');
								//$xcrud_custom_url_keywordsDELETE->start_minimized(true);

								echo $xcrud_custom_url_keywordsDELETE->render('view');
							?>		

							<?php
							if ($device_model == 'UTM5') {
									$xcrud_custom_content_keywords = Xcrud::get_instance();
									$xcrud_custom_content_keywords->table('custom_content_keywords');
									$xcrud_custom_content_keywords->where('cid =', $client_id);
									$xcrud_custom_content_keywords->where('status <>','delete');
									$xcrud_custom_content_keywords->pass_var('cid',$client_id);
									$xcrud_custom_content_keywords->set_var('cidDelete',$client_id);
									$xcrud_custom_content_keywords->unset_numbers();
									$xcrud_custom_content_keywords->unset_view();
									$xcrud_custom_content_keywords->table_name('Add Words to The Content Word List');
									$xcrud_custom_content_keywords->columns('word,rating,status');
									$xcrud_custom_content_keywords->fields('word,rating,status');
									$xcrud_custom_content_keywords->column_width('rating,status','5%');
									$xcrud_custom_content_keywords->highlight('status', '=', 'Active', '#8DED79');
									$xcrud_custom_content_keywords->highlight('status', '=', 'Inactive', '#FF8080');
									$xcrud_custom_content_keywords->relation('rating','keyword_ratings','rating','rating');
									$xcrud_custom_content_keywords->after_update('content_after_update');
									$xcrud_custom_content_keywords->after_insert('content_after_insert');
									$xcrud_custom_content_keywords->after_remove('content_after_remove');	
									$xcrud_custom_content_keywords->relation('status','status_customizations','status','status');	

									echo $xcrud_custom_content_keywords->render('view');
								}
							?>

							<?php
								if ($device_model == 'UTM5') {
									echo '<b>WARNING: The words on this list are very graphic!</b>';
									$xcrud_custom_content_keywordsDELETE = Xcrud::get_instance();
									$xcrud_custom_content_keywordsDELETE->table('custom_content_keywords');
									$xcrud_custom_content_keywordsDELETE->where('cid =', $client_id);
									$xcrud_custom_content_keywordsDELETE->where('status =','delete');
									$xcrud_custom_content_keywordsDELETE->pass_var('cid',$client_id);
									$xcrud_custom_content_keywordsDELETE->pass_var('status','delete');
									$xcrud_custom_content_keywordsDELETE->set_var('cidDelete',$client_id);
									$xcrud_custom_content_keywordsDELETE->unset_numbers();
									$xcrud_custom_content_keywordsDELETE->unset_view();
									$xcrud_custom_content_keywordsDELETE->unset_edit();
									$xcrud_custom_content_keywordsDELETE->table_name('Remove Words from The Content Word List');
									$xcrud_custom_content_keywordsDELETE->columns('word');
									$xcrud_custom_content_keywordsDELETE->fields('word');
									$xcrud_custom_content_keywordsDELETE->after_update('content_after_update');
									$xcrud_custom_content_keywordsDELETE->after_insert('content_after_insert');
									$xcrud_custom_content_keywordsDELETE->after_remove('content_after_remove');								
									$xcrud_custom_content_keywordsDELETE->relation('word','content_keywords','word','word');
									//$xcrud_custom_content_keywordsDELETE->start_minimized(true);

									echo $xcrud_custom_content_keywordsDELETE->render('view');
								}
							?>									

						</div>
					</li>		

					<li class="sky-tab-content-5">
						<div class="typography">
							<h1>Recomendations</h1>
							hello world
						</div>
					</li>									
				</ul>
			</div>
			<!--/ tabs -->
			
		</div>
	</body>
</html>