<?php 
session_start();


if ( !isset($_SESSION['login']) || $_SESSION['login'] !== true) {

if(empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])){

if ( !isset($_SESSION['token'])) {

if ( !isset($_SESSION['fb_access_token'])) {

 header('Location: login/index.php');

exit;
}
}
}
}

?>

<?php
    include('xcrud-1.6.19/xcrud.php');
?>

<?php 
	$db = Xcrud_db::get_instance();
	$query = 'SELECT * FROM users WHERE username = "'.$_SESSION['username'].'"';								
	$db->query($query);
	$res = $db->result();
	$user_id = $res[0]['id'];

	$query = 'SELECT * FROM user_keys WHERE uid = "'.$user_id.'"';
	$db->query($query);
	$res = $db->result();
	$client_id = $res[0]['cid'];
	$group_id = $res[0]['gid'];
	//used for debug
	//echo $client_id; echo '<--client_id<br>';
	//echo $group_id;  echo '<--group_id<br>';
	//echo $user_id;	 echo '<--user_id<br>';

	if ($group_id != 1) {
		header('Location: index.php');
	}
?>

<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8">		
		<title>ADMIN  - ITBiz.co</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="sky-tabs-1.1.2/css/demo.css">
		<link rel="stylesheet" href="sky-tabs-1.1.2/css/font-awesome.css">
		<link rel="stylesheet" href="sky-tabs-1.1.2/css/sky-tabs.css">
		<link rel="stylesheet" href="sky-tabs-1.1.2/css/sky-tabs-red.css">
		
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="css/sky-tabs-ie8.css">
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
			<script src="js/sky-tabs-ie8.js"></script>
		<![endif]-->
	</head>
	
	<body class="bg-red">

		<div class="body">
		
		<header>
			<span class="headerText">
			<?php echo "Welcome:&nbsp;".$_SESSION['username']."&nbsp;&nbsp;&nbsp;&nbsp;"; ?><a class="logoutLinkText" href="login/logout.php">[ Logout? ]</a>
			</span>
			<h1><img src="images/logo.png" alt="ITBiz.co"/></h1>
		</header>

			<!-- tabs -->
			<div class="sky-tabs sky-tabs-pos-top-left sky-tabs-slide-right sky-tabs-response-to-icons">
				<input type="radio" name="sky-tabs" checked id="sky-tab1" class="sky-tab-content-1">
				<label for="sky-tab1"><span><span><i class="fa fa-bolt"></i>Alert</span></span></label>
				
				<input type="radio" name="sky-tabs" id="sky-tab2" class="sky-tab-content-2">
				<label for="sky-tab2"><span><span><i class="fa fa-picture-o"></i>Keywords</span></span></label>
				
				<input type="radio" name="sky-tabs" id="sky-tab3" class="sky-tab-content-3">
				<label for="sky-tab3"><span><span><i class="fa fa-cogs"></i>Einstein</span></span></label>
				
				<input type="radio" name="sky-tabs" id="sky-tab4" class="sky-tab-content-4">
				<label for="sky-tab4"><span><span><i class="fa fa-globe"></i>Newton</span></span></label>
				
				<ul>
					<li class="sky-tab-content-1">					
						<div class="typography">
							<h1>Alert</h1>
							<?php
								$xcrud_alert = Xcrud::get_instance();
								$xcrud_alert->table('alert');
								$xcrud_alert->columns('cid,date,time,ip_addr,device_info,website,website_detail,email_message');
								//relation ( field, target_table, target_id, target_name, where_array, main_table, multi, concat_separator, tree, depend_field, depend_on )
								$xcrud_alert->relation('cid','client','id',array('first_name','last_name'));
								$xcrud_alert->relation('website','alert_websites','website','website');
								$xcrud_alert->relation('website_detail','alert_websites','website_detail','website_detail',null,'','','','','website','website');
								$xcrud_alert->after_insert('send_alert');
								$xcrud_alert->unset_view();
								$xcrud_alert->unset_limitlist();
								$xcrud_alert->order_by('id', 'desc');
								echo $xcrud_alert->render('view');


								$xcrud_alert_websites = Xcrud::get_instance();
								$xcrud_alert_websites->table('alert_websites');
								$xcrud_alert_websites->columns('website,website_detail');
								$xcrud_alert_websites->unset_view();
								$xcrud_alert_websites->unset_sortable();
								echo $xcrud_alert_websites->render('view');
							?>

							<p class="text-right"><em>Find out more about Nikola Tesla from <a href="http://en.wikipedia.org/wiki/Nikola_Tesla" target="_blank">Wikipedia</a>.</em></p>
						</div>
					</li>
					
					<li class="sky-tab-content-2">
						<div class="typography">
							<?php
								echo 'One word and wild char is acceptable e.g. bo*obs and no other special chars<br>';
								$xcrud_url_keywords = Xcrud::get_instance();
								$xcrud_url_keywords->table('url_keywords');
								//$xcrud_url_keywords->unset_limitlist();
								$xcrud_url_keywords->unset_view();
								$xcrud_url_keywords->unset_sortable();
								$xcrud_url_keywords->order_by('id','desc');
								//$xcrud_url_keywords->unset_search();
								$xcrud_url_keywords->start_minimized(true);
								$xcrud_url_keywords->relation('rating','keyword_ratings','rating','rating');
								$xcrud_url_keywords->column_class('rating', 'align-center font-bold');
								$xcrud_url_keywords->column_width('rating','5%');
								echo $xcrud_url_keywords->render('view');
							?>

							<?php
								echo 'One word or two word phrases and no special chars<br>';
								$xcrud_content_keywords = Xcrud::get_instance();
								$xcrud_content_keywords->table('content_keywords');
								//$xcrud_content_keywords->unset_limitlist();
								$xcrud_content_keywords->unset_view();
								$xcrud_content_keywords->unset_sortable();
								$xcrud_content_keywords->order_by('id','desc');
								//$xcrud_content_keywords->unset_search();
								$xcrud_content_keywords->start_minimized(true);
								$xcrud_content_keywords->relation('rating','keyword_ratings','rating','rating');
								$xcrud_content_keywords->column_class('rating', 'align-center font-bold');
								$xcrud_content_keywords->column_width('rating','5%');
								echo $xcrud_content_keywords->render('view');
							?>

							<?php
								$xcrud_keyword_list = Xcrud::get_instance();
								$xcrud_keyword_list->table('keyword_list');
								//$xcrud_keyword_list->unset_limitlist();
								$xcrud_keyword_list->unset_view();
								$xcrud_keyword_list->unset_sortable();
								$xcrud_keyword_list->unset_search();
								$xcrud_keyword_list->table_name('Keyword List Names');
								$xcrud_keyword_list->start_minimized(true);
								echo $xcrud_keyword_list->render('view');
							?>					

							<?php
							/*
								$xcrud_kw_list_assn = Xcrud::get_instance();
								$xcrud_kw_list_assn->table('kw_list_assn');
								$xcrud_kw_list_assn->unset_limitlist();
								$xcrud_kw_list_assn->unset_view();
								$xcrud_kw_list_assn->unset_sortable();
								$xcrud_kw_list_assn->unset_search();
								$xcrud_kw_list_assn->table_name('Keyword List Assignments');
								$xcrud_kw_list_assn->start_minimized(true);
								//relation ( field, target_table, target_id, target_name, where_array, main_table, multi, concat_separator, tree, depend_field, depend_on )
								$xcrud_kw_list_assn->relation('kid','keywords','id','word');
								//$xcrud_kw_list_assn->relation('lid','keyword_list','id','name','list_type="url" AND attrib="default"','','multi');
								$xcrud_kw_list_assn->relation('lid','keyword_list','id','name','attrib="default"','','multi');
								$xcrud_kw_list_assn->before_insert('kw_list_assn_before_insert');
								echo $xcrud_kw_list_assn->render('view');
							*/ /* only saving for refrence - not needed now		*/
							?>	
							
							<?php
								$xcrud_url_scan = Xcrud::get_instance();
								$xcrud_url_scan->table('url_scan');
								//$xcrud_url_scan->unset_limitlist();
								$xcrud_url_scan->unset_view();
								$xcrud_url_scan->unset_print();
								$xcrud_url_scan->unset_sortable();
								//$xcrud_url_scan->unset_search();
								$xcrud_url_scan->relation('cid','client','id',array('first_name','last_name'));
								$xcrud_url_scan->relation('status','status_activated','status','status');
								$xcrud_url_scan->relation('lid','keyword_list','id','name','list_type="url" AND attrib="default"');								
								$xcrud_url_scan->columns('cid,lid,status');
								$xcrud_url_scan->fields('cid,lid,status');
								$xcrud_url_scan->table_name('URL Keyword Scanning Client Config');
								$xcrud_url_scan->start_minimized(true);
								$xcrud_url_scan->after_update('url_after_update');
								echo $xcrud_url_scan->render('view');
							?>

							<?php
								$xcrud_content_scan = Xcrud::get_instance();
								$xcrud_content_scan->table('content_scan');
								//$xcrud_content_scan->unset_limitlist();
								$xcrud_content_scan->unset_view();
								$xcrud_content_scan->unset_print();
								$xcrud_content_scan->unset_sortable();
								//$xcrud_content_scan->unset_search();
								$xcrud_content_scan->relation('cid','client','id',array('first_name','last_name'));
								$xcrud_content_scan->relation('status','status_activated','status','status');
								$xcrud_content_scan->relation('lid','keyword_list','id','name','list_type="content" AND attrib="default"');								
								$xcrud_content_scan->columns('cid,lid,status');
								$xcrud_content_scan->fields('cid,lid,status');
								$xcrud_content_scan->table_name('Content Keyword Scanning Client Config');
								$xcrud_content_scan->start_minimized(true);
								$xcrud_content_scan->after_update('content_after_update');
								echo $xcrud_content_scan->render('view');
							?>																												
						</div>
					</li>
					
					<li class="sky-tab-content-3">
						<div class="typography">
							<h1>Albert Einstein</h1>
							<p>German-born theoretical physicist who developed the general theory of relativity, one of the two pillars of modern physics (alongside quantum mechanics). While best known for his mass–energy equivalence formula E = mc2 (which has been dubbed "the world's most famous equation"), he received the 1921 Nobel Prize in Physics "for his services to theoretical physics, and especially for his discovery of the law of the photoelectric effect". The latter was pivotal in establishing quantum theory.</p>
							<p>Near the beginning of his career, Einstein thought that Newtonian mechanics was no longer enough to reconcile the laws of classical mechanics with the laws of the electromagnetic field. This led to the development of his special theory of relativity. He realized, however, that the principle of relativity could also be extended to gravitational fields, and with his subsequent theory of gravitation in 1916, he published a paper on the general theory of relativity.</p>
							<p>He continued to deal with problems of statistical mechanics and quantum theory, which led to his explanations of particle theory and the motion of molecules. He also investigated the thermal properties of light which laid the foundation of the photon theory of light. In 1917, Einstein applied the general theory of relativity to model the large-scale structure of the universe.</p>
							<p class="text-right"><em>Find out more about Albert Einstein from <a href="http://en.wikipedia.org/wiki/Albert_Einstein" target="_blank">Wikipedia</a>.</em></p>
						</div>
					</li>
					
					<li class="sky-tab-content-4">
						<div class="typography">
							<h1>Isaac Newton</h1>
							<p>English physicist and mathematician who is widely regarded as one of the most influential scientists of all time and as a key figure in the scientific revolution. His book Philosophiæ Naturalis Principia Mathematica ("Mathematical Principles of Natural Philosophy"), first published in 1687, laid the foundations for most of classical mechanics. Newton also made seminal contributions to optics and shares credit with Gottfried Leibniz for the invention of the infinitesimal calculus.</p>
							<p>Newton's Principia formulated the laws of motion and universal gravitation that dominated scientists' view of the physical universe for the next three centuries. It also demonstrated that the motion of objects on the Earth and that of celestial bodies could be described by the same principles. By deriving Kepler's laws of planetary motion from his mathematical description of gravity, Newton removed the last doubts about the validity of the heliocentric model of the cosmos.</p>
							<p class="text-right"><em>Find out more about Isaac Newton from <a href="http://en.wikipedia.org/wiki/Isaac_Newton" target="_blank">Wikipedia</a>.</em></p>
							<p>
						</div>
					</li>					
				</ul>
			</div>
			<!--/ tabs -->
		</div>
	</body>
</html>