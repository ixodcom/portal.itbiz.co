<?php
session_start();
session_destroy();
?><!DOCTYPE html>
<head>
    <title>PHP Login System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
	    <link href="css/style.css" rel="stylesheet" media="screen">
</head>

<body>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <div class="logo">
         <h2><?php include('db.php'); echo $logotxt; ?></h2>
    </div>
    <form class="form-horizontal" id="login_form">
         <h2>Sign IN</h2>

        <div class="line"></div>
        <div class="control-group">
            <input type="text" id="inputEmail" name="username" placeholder="Username">
        </div>
        <div class="control-group">
            <input type="password" id="inputPassword" name="password" placeholder="Password">
        </div> <a class="forgotten-password-link" href="forgot_form.php">Forgot password?</a>
	<a href="registration_form.php"
        class="btn btn-large btn-register">Register</a>

        <button type="submit" class="btn btn-large btn-primary btn-sign-in"
        data-loading-text="Loading...">Sign in</button>
        <div class="messagebox">
            <div id="alert-message"></div>
        </div>

<!--		<div class="social">
		<a href="twitter_connect.php"><img src="img/twitter.png"/></a>
		<a href="facebook_connect.php"><img src="img/fb.png"/></a>
		<a href="google_connect.php?=code"><img src="img/gplus.png"/></a>		
		</div>
Edited out by Pat Holman -->

    </form>
	
</script>
	<script type="text/javascript">
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client:plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
    </script>
    <script>
        $(document).ready(function() {


            $("#login_form").submit(function() {
				$('.social').hide('slow');
                $("#login_form").validate({
                    rules: {
                        username: {
                            required: true
                        },
                        password: {
                            required: true,
                            minlength: 6
                        }
                    },

                    messages: {
                        username: {
                            required: "Enter your username"
                                                    },
                        password: {
                            required: "Enter your password",
                            minlength: "Password must be minimum 6 characters"
                        },
                    },



                    errorPlacement: function(error, element) {
                        error.hide();
						$('.messagebox').hide();
                        error.appendTo($('#alert-message'));
                        $('.messagebox').slideDown('slow');
                       
						
						
                    },
                    highlight: function(element, errorClass, validClass) {
                        $(element).parents('.control-group').addClass('error');
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).parents('.control-group').removeClass('error');
                        $(element).parents('.control-group').addClass('success');
                    }
                });

                if ($("#login_form").valid()) {
                    var data1 = $('#login_form').serialize();
                    $.ajax({
                        type: "POST",
                        url: "login.php",
                        data: data1,
						dataType: 'json',
                        success: function(msg) {
							
                            if (msg.result == 1) {
							$('.messagebox').addClass("success-message");
							$('.message').slideDown('slow');
							$('#alert-message').text("Logged in.. Redirecting");
							
                                $('#login_form').fadeOut(5000);
                                window.location = "../index.php" //edit to app home page by pat holman
                            } else {	
							console.log(msg.result);
							$('.messagebox').hide();
							$('.messagebox').addClass("error-message");
							$('#alert-message').html(msg.result);
							 $('.messagebox').slideDown('slow');
							}
                        }
                    });
                }
                return false;
            });
        });
    </script>
</body>

</html>