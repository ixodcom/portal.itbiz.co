<?php
function publish_action($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE base_fields SET `bool` = b\'1\' WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}
function unpublish_action($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query = 'UPDATE base_fields SET `bool` = b\'0\' WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}

function exception_example($postdata,$primary,$xcrud){
    $xcrud->set_exception('ban_reason','Lol!','error');
    $postdata->set('ban_reason','lalala');
}

function test_column_callback($value, $fieldname, $primary, $row, $xcrud){
    return $value . ' - nice!';
}

function after_upload_example($field, $file_name, $file_path, $params, $xcrud){
    $ext = trim(strtolower(strrchr($file_name, '.')), '.');
    if($ext != 'pdf' && $field == 'uploads.simple_upload'){
        unlink($file_path);
        $xcrud->set_exception('simple_upload','This is not PDF','error');
    }
}

function date_example($postdata,$primary,$xcrud){
    $created = $postdata->get('datetime')->as_datetime();
    $postdata->set('datetime',$created);
}

// functions added by pat holman

function upload_config($xcrud)
{
    if ($xcrud->get('primary'))
    {
        $db = Xcrud_db::get_instance();
        $query =  'UPDATE config SET status="update" WHERE id = ' . (int)$xcrud->get('primary');
        $db->query($query);
    }
}


function send_alert($xcrud)
{
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM alert_notify WHERE cid = "'.$xcrud->get('cid').'"';
    //$query = 'SELECT * FROM alert_notify ';
    //echo $query; echo '<br>';
    $db->query($query);
    $res = $db->result();
    //print_r($res);echo '<br>';
    foreach ($res as $key => $val) {
        $query = 'SELECT * FROM users WHERE id ="'.$res[$key]['uid'].' LIMIT 1"';
        $db->query($query);
        $res_email = $db->result();
        //echo $res_email[0]['email'].' <-- email address<br>';
        $to = $res_email[0]['email'];
        $subject = 'Alert from ITBiz.co- Date: '.$xcrud->get('date').' : Time '.$xcrud->get('time');
        $message = 'Alert Date: '.$xcrud->get('date')."\r\n\r\nTime: ".$xcrud->get('time')."\r\n\r\nIP address: ".$xcrud->get('ip_addr')."\r\n\r\nDevice Information: ".$xcrud->get('device_info')."\r\n\r\n".$xcrud->get('email_message')."\r\n\r\nWebsite:\r\n".$xcrud->get('website')."\r\n\r\nWebsite Details:\r\n".$xcrud->get('website_detail')."\r\n";
        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=iso-8859-1";
        $headers[] = "From: Alert ITBiz.co <alert@itbiz.co>";
        $headers[] = "Bcc: Pat Holman <pat@ixod.com>";
        $headers[] = "Reply-To: Alert ITBiz.co <alert@itbiz.co>";
        $headers[] = "X-Mailer: PHP/".phpversion();
        $retval = mail($to, $subject, $message, implode("\r\n", $headers));
        if( $retval == true ){
            echo "Message sent successfully to: ".$to."<br>";
        }else{
            echo "MESSAGE COULD NOT BE SENT TO: ".$to."<br>";
        }
    } 
}

function whitelist_before_insert($postdata,$xcrud){
    $url = $postdata->get('url_whitelist'); 
    if (strpos($url,'http') !== false){
        $url = parse_url($url);
        $postdata->set('url_whitelist',$url['host']);
    }
}

function whitelist_after_insert($xcrud){
    $client_id = $xcrud->get('cid');
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "url_whitelist" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="url_whitelist"';
        $db->query($query);
    }
}

function whitelist_after_update($xcrud){
    $client_id = $xcrud->get('cid');
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "url_whitelist" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="url_whitelist"';
        $db->query($query);
    }
}

function whitelist_before_remove($primary){
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM url_whitelist WHERE id ="'.$primary.'"';
    $db->query($query);
    $res = $db->result();
    $query = 'INSERT INTO opendns_delete SET cid="'.$res[0]['cid'].'", url="'.$res[0]['url_whitelist'].'", list_name="url_whitelist"';
    $db->query($query);
}

function whitelist_after_remove($primary,$xcrud){
    $client_id = $xcrud->get_var('client_id'); 
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "url_whitelist" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="url_whitelist"';
        $db->query($query);
    }
}

/*
function kw_list_assn_before_insert($xcrud){
    $db = Xcrud_db::get_instance();
    $elm_zero = 0;
    $kw_id = $xcrud->get('kw_list_assn.kid');
    $list_id = $xcrud->get('kw_list_assn.lid');
    $list = explode(',', $list_id);
    foreach ($list as $value) {
        if($elm_zero == 0){
            $elm_zero++;
        }else{
            $query = 'INSERT INTO kw_list_assn SET kid="'.$kw_id.'", lid="'.$value.'"';
            $db->query($query);
        }
    }
}
UNUSED CODE */

function url_after_insert($xcrud){
    $client_id = $xcrud->get('cid');
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "url_scan" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="url_scan"';
        $db->query($query);
    }
}

function url_after_update($xcrud){
    $client_id = $xcrud->get('cid');
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "url_scan" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="url_scan"';
        $db->query($query);
    }
}

function url_after_remove($primary,$xcrud){
    $client_id = $xcrud->get_var('cidDelete'); 
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "url_scan" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="url_scan"';
        $db->query($query);
    }
}

function content_after_insert($xcrud){
    $client_id = $xcrud->get('cid');
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "content_scan" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="content_scan"';
        $db->query($query);
    }
}

function content_after_update($xcrud){
    $client_id = $xcrud->get('cid');
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "content_scan" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="content_scan"';
        $db->query($query);
    }
}

function content_after_remove($primary,$xcrud){
    $client_id = $xcrud->get_var('cidDelete'); 
    $db = Xcrud_db::get_instance();
    $query = 'SELECT * FROM update_status WHERE type = "content_scan" AND cid ="'.$client_id.'"';
    $num_rows = $db->query($query);
    $res = $db->result();
    if( empty( (bool)$res ) )
    {
        $query = 'INSERT INTO update_status SET cid="'.$client_id.'", type="content_scan"';
        $db->query($query);
    }
}